import axios from "axios";
// import { string } from "yargs";

// Get all mission
export const GetAllMission = async (): Promise<any> => {
  const { data } = await axios.get("http://localhost:4000/todo");
  return data;
};

// Get mission by id
export const GetMissionById = async (taskId: string): Promise<any> => {
  const { data } = await axios.get(`http://localhost:4000/todo/${taskId}`);
  return data;
};

// create mission
export const CreatMission = async (listId: string, task: string, finishDate?: Date): Promise<any> => {
  const parameters = { listId: listId, task: task, finishDate: finishDate };

  return await axios.post("http://localhost:4000/todo", parameters);
};

// Delete mission by id
export const DeleteMissionById = async (taskId: string): Promise<any> => {
  const response = await axios.delete(`http://localhost:4000/todo/${taskId}`);
  return response;
};

// Updat value
export const UpMissionById = async (taskId: string, parameters: object): Promise<any> => {
  const UpMission = await axios.put(`http://localhost:4000/todo/${taskId}`, parameters);
  return UpMission;
};

// Change date
export const UpDate = async (taskId: string, finishDate: Date): Promise<any> => {
  const response = await axios.put(`http://localhost:4000/todo/date/${taskId}`, { finishDate: finishDate });
  return response;
};

// Delete date
export const DeletDate = async (taskId: string): Promise<any> => {
  const response = await axios.delete(`http://localhost:4000/todo/date/${taskId}`);
  return response;
};

// Change value
export const UpComplicted = async (taskId: string): Promise<any> => {
  console.log("i stop here");
  const response = await axios.put(`http://localhost:4000/todo/toggle/${taskId}`);
  return response;
};

// Get all list
export const GetAllList = async (): Promise<any> => {
  const response = (await axios.get("http://localhost:4000/todo-list")).data;
  return response;
};

// Get list by id
export const GetListById = async (Listid: string): Promise<any> => {
  const response = await axios.get(`http://localhost:4000/todo-list/${Listid}`);
  return response;
};

// Create list
export const CreatList = async (ListName: string): Promise<any> => {
  const response = await axios.post("http://localhost:4000/todo-list/", { name: ListName });
  return response;
};

// Delete list by id
export const DeleteListById = async (listId: string): Promise<any> => {
  const response = await axios.delete(`http://localhost:4000/todo-list/${listId}`);
  return response;
};
