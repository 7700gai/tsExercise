// import axios from "axios";
import * as api from "./index";
import { describe, test, expect } from "@jest/globals";

describe("ToDo list mission ", () => {
  let listId: string;
  const Name: string = "ljhfkhrd";
  let mission: any;

  test("Create a new list", async () => {
    const response = await api.CreatList(Name);
    listId = response.data.id;
    expect(response.status).toEqual(200);
    expect(response.data.name).toEqual(Name);
  });

  test("Create a new mission", async () => {
    const { data, status } = await api.CreatMission(listId, "TaskId", new Date());
    expect(status).toEqual(200);
    expect(data.listId).toEqual(listId);
    mission = data;
  });

  test("Change value is complicted", async () => {
    const result = await api.UpComplicted(mission.id);
    expect(result.status).toEqual(200);
  });

  test("up mission by id", async () => {
    const response = await api.UpMissionById(mission.id, { listId: listId, task: "changed!" });
    expect(response.status).toEqual(200);
  });

  test("Change date", async () => {
    const time = new Date();
    const request = await api.UpDate(mission.id, time);
    const theTime = (await api.GetMissionById(mission.id)).finishDate;
    expect(request.status).toBe(200);
    expect(time.toISOString()).toEqual(theTime);
  });

  test("Delete date", async () => {
    const before = await api.GetMissionById(mission.id);
    expect(before).toHaveProperty("finishDate");
    const dalete = (await api.DeletDate(mission.id)).status;
    const after = await api.GetMissionById(mission.id);
    expect(dalete).toBeLessThan(300);
    expect(after).not.toHaveProperty("finishDate");
  });

  test("Delete Mission by id", async () => {
    const before = await api.GetMissionById(mission.id);
    const status = (await api.DeleteMissionById(mission.id)).status;
    const after = await api.GetMissionById(mission.id);
    expect(status).toBeLessThan(300);
    expect(after).toEqual("");
  });

  test("Delete list by id", async () => {
    const before = (await api.GetListById(listId)).data;
    const a = await api.DeleteListById(listId);
    expect(a.status).toEqual(200);
    const after = (await api.GetListById(listId)).data;
    expect(after).toEqual("");
  });
});

describe("ToDo list mission ", () => {
  let listId: string;
  const Name: string = "10";
  let mission: any;

  test("Delete list and mission by id", async () => {
    const ListName = await api.CreatList(Name);
    const { data, status } = await api.CreatMission(ListName.data.id, "sagf1hgyryhmhhmg");

    const allTasksArray = await api.GetAllMission();
    let tasksOfaList = [];

    const DaletList = await api.DeleteListById(ListName.data.id);

    allTasksArray.map(async (mission: any) => {
      if (mission.listId == ListName.data.id) {
        tasksOfaList.push(mission);
        await api.DeleteMissionById(mission.id);
        const response = await api.GetMissionById(mission.id);
        expect(response).toEqual("");
      }
    });
  });
});

// describe("ToDo list mission ", () => {
//   let listId: string;
//   const Name: string = "10";
//   let mission: any;

//   test("Delete list and mission by id", async () => {
//     const ListName = await api.CreatList(Name);
//     const { data, status } = await api.CreatMission(ListName.data.id, "sagf1hgyryhmhhmg");

//     const allTasksArray = await api.GetAllMission();
//     let tasksOfaList = [];
//     let missionId = "";

//     allTasksArray.map(async (mission: any) => {
//       if (mission.listId == ListName.data.id) {
//         missionId = mission.id;
//         tasksOfaList.push(mission);
//         await api.DeleteMissionById(mission.id);
//       }
//     });

//     const DaletList = await api.DeleteListById(ListName.data.id);
//     const response = await api.GetMissionById(missionId);
//     expect(response).toEqual("");
//   });
// });
